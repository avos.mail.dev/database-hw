# database-hw

This directory contains an ER diagram, a mysql dump as well as several screenshots.

## Generating the mysql dump

(powershell) .\mysqldump -uusername -ppwd --databases db_name > db.sql

## Brief schema overview

Company table linked to the Department table via a foreign key. Employee table linked to both the Company and 
Department via respective foreign keys.

## Insert

### Company
INSERT INTO `company` VALUES (1,'alg1');

### Department
INSERT INTO `department` VALUES (1,'accounting',1);

### Employee
INSERT INTO `employee` VALUES (1,'Steven',6000.00,'leader',1,1),(2,'Robert',5000.00,'worker',1,1),(3,'Michael',7000.00,'leader',1,1);

## Select

### Get employee names and their respective paychecks.
SELECT employee_name, pay FROM employee;

## Get department heads and calculate their average salaries.

SELECT employee_name FROM employee WHERE rank = "leader"
    GROUP BY employee_name
    UNION ALL
    SELECT AVG(pay) FROM employee WHERE rank = "leader";

## Procedure which calculates the average salary of every employee

delimiter //  
create procedure get_avg_pay()  
-> begin  
-> SELECT AVG(pay) FROM employee;  
-> end //  
call get_avg_pay();
